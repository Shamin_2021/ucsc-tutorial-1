#include <stdio.h>
#include <math.h>

int checkstrong(int x);
void range(int start,int end);

int main(void) {
  int end,start,i,fac=1;
  printf("ENTER START POINT :");
  scanf("%d",&start);
  printf("ENTER END POINT :");
  scanf("%d",&end);
  
  printf("STRONG Numbers between %d and %d are :",start,end);
  range(start,end);

  checkstrong(start);
  
  return 0;
}
void range(int start,int end){
  int i;
  for(i=start;i<=end;i++){
    if(checkstrong(i)){
      printf("\n %d",i);
    };
  }
}
int checkstrong(int x){
  int i,digit,sum=0,df,p = x;
  while(x>0){
    df =1;
    digit = x%10;
    for(i=digit;i>=1;i--){
       df*=i;
    }
    sum += df;
    x = x/10;
  }
  if(p == sum){
    return 1;
  }
  else{
    return 0;
  }
  
}