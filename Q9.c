#include <stdio.h>
#include <math.h>

int checkArmstrong(int x);
void range(int start,int end);

int main(void) {
  int end,start,i;
  printf("ENTER START POINT :");
  scanf("%d",&start);
  printf("ENTER END POINT :");
  scanf("%d",&end);
  
  printf("ARMSTRONG Numbers between %d and %d are :",start,end);
  range(start,end);
  
  return 0;
}
void range(int start,int end){
  int i;
  for(i=start;i<=end;i++){
    if(checkArmstrong(i)){
      printf("\n %d",i);
    };
  }
}
int checkArmstrong(int x){
  int i=10,digit,sum=0,dc,p = x;
  while(x>0){
    digit = x%10;
    dc = pow(digit,3);
    sum += dc;
    x = x/10;
  }
  if(p == sum){
    return 1;
  }
  else{
    return 0;
  }
  
}