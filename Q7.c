#include <stdio.h>

int checkPrime(int x);
void range(int start,int end);


int main(void) {
  int end,start,i;
  printf("ENTER START POINT :");
  scanf("%d",&start);
  printf("ENTER END POINT :");
  scanf("%d",&end);
  
  printf("PRIME Numbers between %d and %d are :",start,end);
  range(start,end);
  
  return 0;
}

void range(int start,int end){
  int i;
  for(i=start;i<=end;i++){
    if(checkPrime(i)){
      printf(" %d , ",i);
    };
  }
}

int checkPrime(int x){
  int factors =0;

  for(int i=1;i<=x;i++){
    if(x%i == 0){
      factors++;
    }
  }
  if (factors==2){
    return 1;
  }else {
    return 0;
  }
}