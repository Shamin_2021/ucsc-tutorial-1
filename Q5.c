#include <stdio.h>

void checkPrime(int x);

int main(void) {
  int x;
  printf("enter the number :");
  scanf("%d",&x);
  checkPrime(x);
  return 0;
}

void checkPrime(int x){
  int factors =0;

  for(int i=1;i<=x;i++){
    if(x%i == 0){
      factors++;
    }
  }
  if (factors==2){
    printf("the number is a PRIME ");
  }else {
    printf("the number is a NON-PRIME ");
  }
}