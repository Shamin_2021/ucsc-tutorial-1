#include <stdio.h>

void checkOddEven(int x);

int main(void) {
  int x;
  printf("enter Number : ");
  scanf("%d",&x);
  checkOddEven(x);
  return 0;
}

void checkOddEven(int x){
  if(x%2 == 0){
    printf("number %d is even",x);
  }
  else{
    printf("number %d is odd",x);
  }
}