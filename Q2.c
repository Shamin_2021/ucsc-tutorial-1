#include <stdio.h>
#define PI 3.14

float diameter(float radius);
float circumference(float radius);
float area(float radius);

int main(void) {

  float Radius,d,c,a;
  printf("Enter the radius :");
  scanf("%f", &Radius);

  d = diameter(Radius);
  a = area(Radius);
  c = circumference(Radius);

  printf("\ndiameter : %.1f",d);
  printf("\nArea : %.2f",a);
  printf("\ncircumference : %.2f",c);
  return 0;
}

float diameter(float radius){
  return 2*radius;
}
float circumference(float radius){
  return 2*radius*PI;
}
float area(float radius){
  return PI*radius*radius;
}